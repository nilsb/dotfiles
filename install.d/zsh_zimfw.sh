#!/bin/sh

zimfw_file="${XDG_DATA_HOME:-$HOME/.local/share}/zsh/zim/zimfw.zsh"

if [ -f "$zimfw_file" ]; then
	echo 'Skiping zimfw install: already installed.'
else
	echo 'Installing zimfw...'

	curl --fail --location --output "$zimfw_file" --create-dirs \
		https://github.com/zimfw/zimfw/releases/latest/download/zimfw.zsh

	zsh "$zimfw_file" install
fi
