#!/bin/sh
#
# See https://github.com/nextcloud/desktop/issues/1011#issuecomment-510153674
# Adjusted for use with keepassxc via org.freedesktop.secrets
#
############################################
COLLECTION=Passwords  # Name of collection storing nextcloud client password
TIMEOUT=3s            # Time to wait between each check
############################################

isDbusServiceAvailable() {
  qdbus org.freedesktop.secrets > /dev/null 2>&1
}

isLocked() {
  [ "$(qdbus org.freedesktop.secrets "/org/freedesktop/secrets/collection/$COLLECTION" Locked)" = "true" ]
}

while ! isDbusServiceAvailable || isLocked; do
    sleep "$TIMEOUT"
done

exec nextcloud
