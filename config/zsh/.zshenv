# Start configuration added by Zim install {{{
#
# User configuration sourced by all invocations of the shell
#

# Define Zim location
: ${ZIM_HOME=${XDG_DATA_HOME:-${HOME}/.local/share}/zsh/zim}
# }}} End configuration added by Zim install


# load systemd's environment.d env vars
while IFS='' read -r line; do
  eval export "$line"
done < <(/run/current-system/sw/lib/systemd/user-environment-generators/30-systemd-environment-d-generator)

# Add user's local bin directories to $PATH
for dir in \
  "$HOME/.local/share/npm/bin" \
  "$HOME/.jbang/bin" \
  "$HOME/.pub-cache/bin" \
  "$HOME/.local/bin" \
; do
  [[ ":$PATH:" == *":$dir:"* ]] || PATH="$dir:$PATH"
done

# less: activate mouse scrolling and disable histfile
export LESS="R --mouse"
export LESSHISTFILE="-"
export SYSTEMD_LESS='FRSM --mouse'
