# start X11 if on first tty and $DISPLAY not set
if [[ -z $DISPLAY && $XDG_VTNR -eq 1 ]]; then
  XDG_SESSION_TYPE='x11' XDG_CURRENT_DESKTOP='x-custom:X-NIXOS-SYSTEMD-AWARE' exec xsession-wrapper startx ${XDG_CONFIG_HOME:-$HOME/.config}/X11/xinitrc
fi
