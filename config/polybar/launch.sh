#!/bin/sh

if [ "$#" -eq 0 ]; then
    set -- 'top' 'bottom'
fi

for bar in "$@"; do
    systemctl --user reload-or-restart "polybar@$bar.service"
done
