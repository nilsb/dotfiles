#!/bin/sh

pkg_manager="$1"

nice -n10 "$(dirname "$0")/pkgupdates-status-$pkg_manager.sh"
case "$?" in
    "0")  echo '';;
    "1")  echo '󰦗';;
    "2")  echo '';;
    "3")  echo '󰚰';;
esac
