#!/bin/sh

set --
set -- '--catalog'

case "$(systemctl show -pActiveState --value nixos-upgrade.service)" in
	'activating')  set -- "$@" '--follow'    ;;  # updates running:       follow log
	*)             set -- "$@" '--pager-end' ;;  # not running / unknown: jump to end
esac

set -- "$@" _SYSTEMD_INVOCATION_ID="$(systemctl show -p InvocationID --value nixos-upgrade.service)"

export SYSTEMD_LESS='RSMK'

exec journalctl "$@"
