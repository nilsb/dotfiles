#!/bin/sh
# based on https://gist.github.com/olmokramer/b28ff8ed5fd366e3ebb23b79915ec850

hash herbstclient xrandr

print_tag() {
	icon="$1"
	color="$2"
	invert="$3"

	                    printf '%%{O%d}' '2'
	[ -n "$invert" ] && printf '%%{R}'
	[ -n "$color"  ] && printf '%%{F%s}' "$color"
	                    printf '%s'      "$icon"
	[ -n "$color"  ] && printf '%%{F%s}' '-'
	[ -n "$invert" ] && printf '%%{R}'
	                    printf '%%{O%d}' '1'
}

print_tags() {
	i=0
	for tag in $(herbstclient tag_status "$1"); do
		name=${tag#?}
		state=${tag%"$name"}

		printf '%%{A1:herbstclient use_index %s:}' "$i"

		case "$state" in
			'.')
				print_tag '%{O5}%{O5}' '#373B41'
				;;
			':')
				print_tag ' ' '#373B41'
				;;
			'+')
				print_tag ' ' '#707880'
				;;
			'#')
				print_tag ' '
				;;
			'-')
				print_tag ' ' '#707880'
				;;
			'%')
				print_tag ' ' '#C0CDD0'
				;;
			'!')
				print_tag "%{O1}%{T$FONT_TEXT}!%{T$FONT_ICONS}%{O4}" '' 1
				;;
			*)
				print_tag "$name%{O-3}"
				;;
		esac

		printf '%%{A}'

		i=$((i+1))
	done
	printf '\n'
}

geom_regex='[[:digit:]]\+x[[:digit:]]\++[[:digit:]]\++[[:digit:]]\+'
geom=$(xrandr --query | grep "^${MONITOR:-'.* primary'}" | grep -o "$geom_regex")
monitor=$(herbstclient list_monitors | grep "$geom" | cut -d: -f1)

print_tags "$monitor"

IFS="$(printf '\t')" herbstclient --idle | while read -r hook _args; do
	case "$hook" in
		tag*)
			print_tags "$monitor"
			;;
	esac
done
