#!/bin/sh

updates_available() {
	flatpak --system --user update --noninteractive --no-pull --no-deploy | grep --quiet '^Updating '
}

case "$(systemctl --user show -pActiveState --value flatpak-update-download.service)" in
	'inactive')              # last download successful
		if updates_available
			then exit 3        # updates available
			else exit 0        # no updates available
		fi;;
	'activating')  exit 1;;  # download running
	'failed')      exit 2;;  # error while downloading
	*)             exit 2;;  # unknown
esac
