#!/bin/sh

pkg_manager="$1"
set --

#teminal emulator
set -- "alacritty" "--class=pkgupdates" "-e"

# update script
set -- "$@" "$(dirname "$0")/pkgupdates-action-$pkg_manager.sh"

exec nice -n10 "$@"
