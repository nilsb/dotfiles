#!/bin/sh

case "$(systemctl show -pActiveState --value nixos-upgrade.service)" in
	'inactive')    exit 0;;  # last update successful
	'activating')  exit 1;;  # updates running
	'failed')      exit 2;;  # error while updating
	*)             exit 2;;  # unknown
esac
