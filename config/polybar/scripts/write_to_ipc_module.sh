#!/bin/sh

polybar_pid="$1"
polybar_module_name="$2"
shift 2

{ echo '󰔟'; "$@"; } | while IFS='' read -r output; do
	polybar-msg -p "$polybar_pid" action "$polybar_module_name" send "$output"
done
