#!/bin/sh

first_match() {
	if [ -e "$1" ]; then
		printf '%s' "${1##*/}"
	fi
}

INTERFACE_ETHERNET="$(first_match '/sys/class/net/e'*)"
INTERFACE_WIFI="$(first_match '/sys/class/net/w'*)"

BATTERY="$(first_match '/sys/class/power_supply/B'*)"
BATTERY_ADAPTER="$(first_match '/sys/class/power_supply/A'*)"

NETWORK_UI="nm-connection-editor"

PULSEAUDIO_SINK="$(pactl get-default-sink)"

BACKLIGHT_CARD="$(first_match '/sys/class/backlight/'*)"

export INTERFACE_ETHERNET INTERFACE_WIFI BATTERY BATTERY_ADAPTER NETWORK_UI PULSEAUDIO_SINK BACKLIGHT_CARD
env | grep -E '^(INTERFACE_ETHERNET|INTERFACE_WIFI|BATTERY|BATTERY_ADAPTER|NETWORK_UI|PULSEAUDIO_SINK|BACKLIGHT_CARD)'
