" vim:ft=vim
set background=dark
let g:base16colorspace=256

syntax on
set tabstop=4 softtabstop=0 noexpandtab shiftwidth=4

set number relativenumber

set splitbelow splitright

set breakindent showbreak=⤥\  breakindentopt=shift:-2
set cursorline cursorlineopt=number,line

set mouse=a
"set whichwrap+=<,>,h,l,[,]

set clipboard+=unnamedplus

set hidden

" set spell spelllang=de,en_us

" Save netrw data in $XDG_DATA_HOME
let g:netrw_home = '$XDG_DATA_HOME/nvim'

" Load external changes on buffer focus
autocmd FocusGained,BufEnter * :checktime


" Install vim-plug if not found
if empty(glob(stdpath('data').'/site/autoload/plug.vim'))
  execute '!curl -fLo '.stdpath('data').'/site/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('$XDG_DATA_HOME/nvim-plugged')
    Plug 'ap/vim-buftabline'
    Plug 'Shougo/vimproc.vim', {'do' : 'make'}
    Plug 'tpope/vim-surround'
    Plug 'tpope/vim-repeat'
    Plug 'airblade/vim-gitgutter'
    Plug 'majutsushi/tagbar'

    " CLI bindings
    Plug 'tpope/vim-fugitive'
    Plug 'timonv/vim-cargo', {'do': 'curl -fL https://github.com/timonv/vim-cargo/pull/7.patch \| git apply'}

    " language support
    Plug 'sheerun/vim-polyglot'
    "Plug 'craigemery/vim-autotag'    " disabled due to error on startup
    Plug 'vim-scripts/TagHighlight'
    Plug 'potatoesmaster/i3-vim-syntax'
    Plug 'scy/vim-remind'
    Plug 'dart-lang/dart-vim-plugin' " until vim-polyglot updates
    Plug 'lervag/vimtex'
    Plug 'whybin/alloy.vim'

    " vscode plugin support
    Plug 'neoclide/coc.nvim', {'branch': 'release'}

    " status bar
    Plug 'itchyny/lightline.vim'

    " theme
    Plug 'sainnhe/everforest'

    " display keybinds
    Plug 'folke/which-key.nvim'

    " todo list
    Plug 'nvim-lua/plenary.nvim'
    Plug 'folke/todo-comments.nvim'
call plug#end()

" todo list settings
lua require'todo-comments'.setup{}
nnoremap <silent> <leader>t <cmd>TodoLocList<CR>


" Theme settings

if has('termguicolors')
    set termguicolors
endif
set background=dark
let g:everforest_background = 'soft'
let g:everforest_enable_italic = 1
let g:everforest_better_performance = 1
colorscheme everforest

set noshowmode
let g:lightline = {
\    'colorscheme': 'everforest'
\}

" set Colours of tab completion
hi Pmenu ctermbg=Black ctermfg=White
hi PmenuSel ctermbg=DarkGray ctermfg=White
hi PmenuSbar ctermbg=DarkGray ctermfg=White

" Show Whitespace
set list
set listchars=tab:→\ ,trail:·,nbsp:%,extends:\>,precedes:\<
hi NonText ctermfg=DarkGray
hi Whitespace ctermfg=DarkGray


" CoC default options
source $XDG_CONFIG_HOME/nvim/init.vim.d/coc.vim

" CoC keybinds
"  scroll in floating windows
noremap <nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1, 5) : "\<C-f>"
noremap <nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0, 5) : "\<C-b>"
inoremap <nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
"  Alt-Return for language server suggestions
command! -nargs=* -range CocAction :call CocActionAsync('codeAction', <f-args>)
noremap <M-CR> :call CocActionAsync('codeAction')<CR>
"  <Leader>j/k for next/prev lint
nnoremap <silent> <Leader>j :call CocAction('diagnosticNext')<cr>
nnoremap <silent> <Leader>k :call CocAction('diagnosticPrevious')<cr>
"  <Leader>e for explorer
nnoremap <silent> <Leader>e :CocCommand explorer<cr>

" CoC Extensions
let g:coc_global_extensions = [
\   'coc-css',
\   'coc-clangd',
\   'coc-diagnostic',
\   'coc-eslint',
\   'coc-explorer',
\   'coc-flutter',
\   'coc-go',
\   'coc-golines',
\   'coc-html',
\   'coc-java',
\   'coc-json',
\   'coc-ltex',
\   'coc-omnisharp',
\   'coc-python',
\   'coc-rust-analyzer',
\   'coc-sh',
\   'coc-texlab',
\   'coc-tsserver',
\   'coc-vimlsp',
\   'coc-vimtex',
\   'coc-yaml',
\]


" Other Options

"  format rust code while saving
let g:rustfmt_autosave = 1
